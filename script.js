

"use strict";
const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    },

    {
        author: "Стивен Кинг",
        price: 24
    }
];


const booksList = document.createElement('ul');

function errorMsg(book) {

    const keys = Object.keys(book);

    if (!keys.includes('author')) {
        return `Author in ${book.name}`;
    } else if (!keys.includes('name')) {
        return `There are ${book.author} author and price is ${book.price} dollars, but Name in this`;
    } else if (!keys.includes('price')) {
        return `Price in ${book.name}`;
    }

}

function createBook(book) {

    if (Object.keys(book).length === 3) {
        const bookMarkup = `<li><p>${book.author}</p><p>${book.name}</p><p>${book.price}</p></li>`;
        booksList.insertAdjacentHTML("afterbegin", bookMarkup);
    } else {
        throw new Error(`This is error! ${errorMsg(book)} book is empty`);
    }
}

function createBooksList(booksArr) {
    const rootRef = document.querySelector('#root');

    booksArr.forEach(book => {
        try {
            createBook(book);
        } catch (e) {
            console.error(e.message);
        }
    })

    rootRef.append(booksList);
}

createBooksList(books);
